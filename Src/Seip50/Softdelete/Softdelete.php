<?php
namespace App\Seip50\SoftDelete;
use PDO;
class Softdelete {
    public  $id = '';
    public $data = '';
    public  $title = '';
    public  $model = '';
    public $serial = '';
    public  $price = '';
    public  $color = '';
    public  $p_date = '';
    public  $user = 'root';
    public  $pw = '';
    public  $conn = '';
    public $isdeleted = '1';
    
    
    public function __construct() {
       
       session_start();
       $this->conn = new PDO("mysql:host=localhost;dbname = softdelete", $this->user, $this->pw);
    }
    
    
 
    public function prepare($data=''){
        
        if (array_key_exists('name', $data)){
            $this->title = $data['name'];
        }
        if (array_key_exists('model', $data)){
            $this->model = $data['model'];
        }
        if (array_key_exists('sl', $data)){
            $this->serial = $data['sl'];
        }
        if (array_key_exists('color', $data)){
            $this->color = $data['color'];
        }
        if (array_key_exists('price', $data)){
            $this->price = $data['price'];
        }
        if (array_key_exists('pdate', $data)){
            $this->p_date = $data['pdate'];
        }
        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }
        
        return $this;
    }
    
    public function index(){
        try{
            $query = "SELECT * FROM `softdelete`.`info` WHERE isDeleted = 0";
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $this->conn->prepare($query);
            $q->execute();
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo "error". $ex->getMessage();
        }  
        return $this->data;   
    }
    
    public function store(){
        try{
            
            $query = "INSERT INTO `softdelete`.`info` (title,model,sl,color,price,pdate) "
                    . "VALUES (:t,:m,:sl,:cl,:p,:pdate)";
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $this->conn->prepare($query);
            $q->execute(array(
                ':t' => $this->title,
                ':m' => $this->model,
                ':sl' => $this->serial,
                ':cl' => $this->color,
                ':p' => $this->price,
                ':pdate' => $this->p_date  
            ));
            header('location:index.php');
            $_SESSION['Message'] = "<h4>Data Added Successfully.</h4>";
             
        } catch (Exception $ex) {
            echo "Error". $ex->getMessage();
        }
    }
    
    public function show(){
        try{
            $query = "SELECT * FROM `softdelete`.`info` WHERE `info`.id=". $this->id;
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);            
            $q = $this->conn->query($query) or die(mysql_error());     
            $row = $q->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            echo "Error". $ex->getMessage();
        }
        return $row;
    }
    
    public function update(){
        try{
            $query = "UPDATE `softdelete`.`info` SET "
                    . "`title` = '$this->title',"
                    . "`model` = '$this->model',"
                    . "`sl` = '$this->serial',"
                    . "`color` = '$this->color',"
                    . "`price` = '$this->price',"
                    . "`pdate` = '$this->p_date' WHERE `info`.`id`=".$this->id;
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $this->conn->prepare($query);
            $q->execute();
            header('location:index.php');
            $_SESSION['Message'] = "<h4>Data Update Successfully</h4>";
        } catch (Exception $ex) {
            echo "Error". $ex->getMessage();
        }
    }
    
    public function delete() {
        try{
            $query = "DELETE FROM `softdelete`.`info` WHERE `info`.`id` =" . $this->id;
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $this->conn->prepare($query);
            $q->execute();
            header('location:trash.php');
            $_SESSION['Message'] = "<h4>Data Delete Successfully.</h4>";
        } catch (Exception $ex) {
            echo "Error". $ex->getMessage();
        }
    }
    
    public function trash_index() {
        try{
            $query = "SELECT * FROM `softdelete`.`info` WHERE isDeleted = 1";
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $this->conn->prepare($query);
            $q->execute();
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (Exception $ex) {
            echo "error". $ex->getMessage();
        }  
        return $this->data; 
    }
    public function trashdelete() {

        try{
            $query = "UPDATE softdelete.info SET isDeleted = :t WHERE id= :id";
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $this->conn->prepare($query);
            $q->execute(array(
                ':t' => '1',
                ':id' => $this->id
            )) or die(mysql_error());
            header('location:index.php');
            $_SESSION['Message'] = "<h4>Temporary Data Deleted.Check Your Trash.</h4>";
        } catch (Exception $ex) {
            echo "Error". $ex->getMessage();
        }
    }
    public function trashrestore() {
         try{
            $query = "UPDATE softdelete.info SET isDeleted = :t WHERE id= :id";
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $q = $this->conn->prepare($query);
            $q->execute(array(
                ':t' => '0',
                ':id' => $this->id
            )) or die(mysql_error());
            header('location:index.php');
            $_SESSION['Message'] = "<h4>Data Restore Successfully.</h4>";
        } catch (Exception $ex) {
            echo "Error". $ex->getMessage();
        }
    }
    
}
