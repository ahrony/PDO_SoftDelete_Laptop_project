<?php
include_once ('../../../vendor/autoload.php');
use App\Seip50\SoftDelete\Softdelete;

$obj = new Softdelete();
$data = $obj->trash_index();
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
<body>
    <?php include_once ("./header.php");?>
    <div>
            <h3>Trash Laptop Data List</h3>

<?php
    if(isset($_SESSION['Message']) && !empty($_SESSION['Message'])){
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }
?>

        <table>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Model</th>
            <th>Serial</th>
            <th>Color</th>
            <th>Price</th>
            <th>Purchase Date</th>
            <th colspan="3">Action</th>
          </tr>
        <?php
            if(isset($data) && !empty($data)){
            foreach ($data as $item){ ?>
          
                <tr>
                  <td><?php echo $item['id']; ?></td>
                  <td><?php echo $item['title']; ?></td>
                  <td><?php echo $item['model']; ?></td>
                  <td><?php echo $item['sl'] ;?></td>
                  <td><?php echo $item['color']; ?></td>
                  <td><?php echo $item['price']; ?></td>
                  <td><?php echo $item['pdate'];?></td>
                  <td><a href="trashrestore.php?id=<?php echo $item['id'] ?>">Restore</a></td>
                  <td><a href="delete.php?id=<?php echo $item['id'] ?>">Delete</a></td>
                  
                  
                </tr> <?php }} ?>
        </table>
    </div>
</body>
</html>